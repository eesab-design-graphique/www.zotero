
 <?php

 include('inc/variables.php');
 include('inc/functions.php');
 include('inc/head.php');

   $url = file_get_contents('https://api.zotero.org/groups/2249230/items?limit=1000');

   $array = json_decode($url, TRUE);

   $items = array_merge($array);

   $allTags = [];
   $authors = [];
   foreach ($items as $item) {
     $tags = $item['data']['tags'];
     foreach ($tags as $tag) {
       if ($tag['tag'] != 'notShowed' || $tag['tag'] != 'cambreOk' || $tag['tag'] != 'pourMoi') {
         array_push($allTags, $tag['tag']);
       }
     }
   }

   ?>
   <div class="wtf">
     ?
    <div class="infos">
      <p><strong>wwwahou </strong> regroupe une sélection de site web des étudiants en Bachelor de l'option Arts Numériques de l'école Arts² à Mons dans le cadre du cours de culture web. Ils ont chacun été choisis car ils représentent un intérêt en terme de design, artistique ou autre.</p>
      <p>Développé grace à l'API de <a href="http://zotero.org/">Zotero</a>.</p>
      <p><a href="https://gitlab.com/EtienneOz/wwwahou/">sources</a> </p>
    </div>
   </div>
   <?php

   $allTags = array_unique($allTags);

    echo '<ul class="tags">';
      echo '<li><h1>Wwwahou </h1></li>';
      foreach ($allTags as $tag) {
        if ($tag != 'notShowed' && $tag != 'cambreOk' && $tag != 'pourMoi' && substr($tag, 0, 4) != 'auth') {
        echo '~ <li class="tag '. $tag .'">' . $tag . '</li> ';
        }
      }
    echo '</ul>';
    echo '<div class="items">';
     foreach ($items as $item) {
       $data = $item['data'];
       if (is_array($data) && in_array('webpage', $data)) {
         $title = $data['title'];
         $url = $data['url'];
         $date = substr($data['dateAdded'], 0, 10);
         $abstract = $data['abstractNote'];
         $tags = $data['tags'];
         if ($url) {
           # code...
           echo '<div class="item';
           foreach ($tags as $tag) {
            if ($tag['tag'] != 'notShowed' && $tag['tag'] != 'cambreOk' && $tag['tag'] != 'pourMoi' && substr($tag['tag'], 0, 4) != 'auth') {
              echo ' ' . $tag['tag'];
             }
           }
           echo '">';
             echo '<span class="infos">';
               echo '<span>' . $date . ' </span>';
               echo '<a href=" ' . $url . '" target="_blank">' . ($title ? $title : '----') . '</a>';
               echo '<sup>';
                 foreach ($tags as $tag) {
                   if ($tag['tag'] != 'notShowed' && $tag['tag'] != 'cambreOk' && $tag['tag'] != 'pourMoi' && substr($tag['tag'], 0, 4) != 'auth') {
                     echo ' <span>' . $tag['tag'] . '</span>';
                   }
                 }
               echo '</sup>';
               foreach ($tags as $tag) {
                 if (substr($tag['tag'], 0, 4) == 'auth') {
                   echo '<span class="author"> proposé par ' . substr($tag['tag'], 7) . '</span>';
                 }
               }
             echo '</span>';
             echo ' <p>'. $abstract . '</p>';
             echo '<div class="iframe"><iframe data-src="'. $url .'"></iframe></div>';
           echo '</div>';
         }
       }
     }
   echo '</div>';

   include('inc/foot.php');

  ?>
