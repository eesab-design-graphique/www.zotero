
 <?php

  include('inc/variables.php');
  include('inc/head.php');

  $group_id = "4798814";

  $url = file_get_contents('https://api.zotero.org/groups/' . $group_id . '/items?limit=1000');

  $array = json_decode($url, TRUE);
  $items = array_merge($array);
?>

<h1>ZEESABDG</h1>

<?php echo $items[0]['library']['name']; ?>

<?php foreach ($items as $item) {
  $data = $item['data'];
  $title = $data['title'];
  $url = $data['url']; 
?>

<p>title: <a href="<?= $url ?>"><?= $title ?></a></p>

<?php } ?>

<?php
  include('inc/foot.php');
?>
